# VocabularyTrial

A tool for managing vocabulary databases and randomly generating vocabulary exercises based on the user's selection.

## Download (Windows only)
### Latest build download (Build version 1.0.0):
- https://www.mediafire.com/file/7dv546yy1m2np7l/VocabularyTrialMaven-1.0.0.jar/file

### Legacy build downloads:
None so far

## Installation
- Make sure you have a JRE/JDK version 16 or higher installed on your system
- Download the desired application version from the provided download links
- Execute the application by simply double-clicking the application jar file
